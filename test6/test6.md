﻿﻿﻿﻿﻿﻿﻿# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计
**姓名：钟梓萌**

**学号：202010414428**

**班级：20级软工四班**

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
- 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
- 设计权限及用户分配方案。至少两个用户。
- 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
- 设计一套数据库的备份方案。

## 实验内容
1. 确定表结构和业务逻辑 

   ​		首先需要确定商品销售系统的核心功能和数据存储需求。我们假设该系统包括4张表：订单表、客户表、商品表和库存表。订单表记录了每个订单的详细信息，客户表保存客户基本信息，商品表保存商品信息，库存表记录商品的库存量和当前价格。同时需要设计复杂的业务逻辑，比如订单退款/退货流程、优惠券/促销活动等。

   

2. 设计表空间

   ​		本次使用两个表空间：一个用于存储数据文件，另一个用于存储索引文件。由于不同的表大小和访问频率不同，在创建表时需要指定所在的表空间。所以创建了两个表空间，一个是用于存储数据的sales_tablespace，另一个是用于存储索引的indexs_tablespace。其中，每个表空间都指定了一个数据文件（DATAFILE），并且设置了初始大小（SIZE）、自动扩展（AUTOEXTEND）、下一个增长阶段的大小（NEXT）和最大大小（MAXSIZE）。

   ```sql
   --创建表空间
   CREATE TABLESPACE sales_tablespace
     DATAFILE 'sales_tablespace.dbf'
     SIZE 100M
     AUTOEXTEND ON
     NEXT 10M
     MAXSIZE UNLIMITED;
   
   CREATE TABLESPACE indexs_tablespace
     DATAFILE 'indexs_tablespace.dbf'
     SIZE 50M
     AUTOEXTEND ON
     NEXT 10M
     MAXSIZE UNLIMITED;
   -- 分配表空间给用户
   ALTER USER sale_admin DEFAULT TABLESPACE sales_tablespace;
   ALTER USER sale_admin DEFAULT TABLESPACE indexs_tablespace;
   ```

   ![](3.png)

   ![](4.png)

   ```sql
   -- 授予表空间权限给 sale_admin 用户
   ALTER USER sale_admin QUOTA UNLIMITED ON sales_tablespace;
   ALTER USER sale_admin QUOTA UNLIMITED ON temp_tablespace;
   ```

   ![](5.png)

   

   接下来创建了四个表。PRODUCTS表包含产品信息，其中包括产品ID（productid）、产品名称（productname）、价格（price）和数量（quantity）等字段；

   ORDERS表包含订单信息，其中包括订单ID（orderid）、下单日期（orderdate）和顾客ID（customerid）等字段，同时还定义了一个外键引用CUSTOMER表；

   ORDERTETAILS表包含订单细节信息，其中包括订单ID（orderid）、产品ID（productid）、产品数量（quantity）和产品价格（price）等字段，同时还定义了一个复合主键（PRIMARY KEY）来保证订单和产品之间的唯一性；

   CUSTOMER表包含顾客信息，其中包括顾客ID（customerid）、顾客姓名（customername）和电子邮件地址（email）等字段。

      ``` sql
      -- 创建商品表并指定表空间
      CREATE TABLE PRODUCTS (
        productid   NUMBER PRIMARY KEY,
        productname VARCHAR2(100) NOT NULL,
        price        NUMBER(10, 2) NOT NULL,
        quantity     NUMBER
      )
      TABLESPACE sales_tablespace;
      
      -- 创建订单表并指定表空间
      CREATE TABLE ORDERS (
        orderid    NUMBER PRIMARY KEY,
        orderdate  DATE NOT NULL,
        customerid NUMBER REFERENCES customer(customerid)
      )
      TABLESPACE sales_tablespace;
      
      -- 创建订单明细表并指定表空间
      CREATE TABLE ORDERTETAILS (
        orderid   NUMBER REFERENCES orders(orderid),
        productid NUMBER REFERENCES products(productid),
        quantity   NUMBER,
        price      NUMBER(10, 2),
        PRIMARY KEY (orderid, productid)
      )
      TABLESPACE sales_tablespace;
      
      -- 创建用户表并指定表空间
      CREATE TABLE CUSTOMER (
        customerid   NUMBER PRIMARY KEY,
        customername VARCHAR2(100) NOT NULL,
        email         VARCHAR2(100)
      )
      TABLESPACE sales_tablespace;
      ```

   ![](6.png)
   ![](7.png)
   ![](8.png)
   ![](9.png)

   授予了名为`sale_admin`的用户对四个表(PRODUCTS、ORDERS、ORDERTETAILS和CUSTOMER)的SELECT、INSERT、UPDATE和DELETE权限。

   ```
   GRANT SELECT, INSERT, UPDATE, DELETE ON products TO sale_admin;
   GRANT SELECT, INSERT, UPDATE, DELETE ON ORDERS TO sale_admin;
   GRANT SELECT, INSERT, UPDATE, DELETE ON ORDERTETAILS TO sale_admin;
   GRANT SELECT, INSERT, UPDATE, DELETE ON CUSTOMER TO sale_admin;
   ```

   ![](10.png)

   插入数据，每张表三万条数据，四张表共12万条数据。

   首先向PRODUCTS表插入虚拟数据，这些数据包括产品ID、产品名称、价格和数量。具体流程如下：

   - 使用FOR循环语句，从1到30,000遍历每个整数i。
   - 在每个迭代中，使用INSERT INTO语句将一条新记录插入PRODUCTS表中，其中包含productid（即i）、productname（格式为“Product i”）、price（在10到100之间随机生成）和quantity（在1到100之间随机生成）。还使用SELECT语句从dual表中选择一行，同时使用子查询检查是否已经存在具有相同productid的记录。如果存在，则不执行插入操作。
   - 为了避免过大的事务大小，每100次迭代时进行一次COMMIT提交。

   ```
   -- 插入虚拟数据到products表
   BEGIN
     FOR i IN 1..30000 LOOP
       INSERT INTO products (productid, productname, price, quantity)
       SELECT i AS productid, 'Product ' || i AS productname, ROUND(DBMS_RANDOM.VALUE(10, 100), 2) AS price, ROUND(DBMS_RANDOM.VALUE(1, 100)) AS quantity
       FROM dual
       WHERE NOT EXISTS (SELECT 1 FROM products WHERE productid = i);
     
       -- Commit periodically to avoid excessive transaction size
       IF MOD(i, 100) = 0 THEN
         COMMIT;
       END IF;
     END LOOP;
     
     COMMIT;
   END;
   /
   ```

   ![](11.png)

   会向CUSTOMER表插入虚拟数据，具体流程如下：

   - 使用FOR循环语句，从1到30,000遍历每个整数i。
   - 在每个迭代中，使用INSERT INTO语句将一条新记录插入CUSTOMER表中，其中包含customerid（即i）、customername（格式为“Customer i”）和email（格式为“customeri@example.com”）。还使用SELECT语句从dual表中选择一行，同时使用子查询检查是否已经存在具有相同customerid的记录。如果存在，则不执行插入操作。
   - 不需要在循环外显式提交事务，因为每次循环都是一个单独的事务

   ```
   -- 插入虚拟数据到customers表
   BEGIN
     FOR i IN 1..30000 LOOP
       INSERT INTO customer (customerid, customername, email)
       SELECT i AS customerid, 'Customer ' || i AS customername, 'customer' || i || '@example.com' AS email
       FROM dual
       WHERE NOT EXISTS (
         SELECT 1
         FROM customer
         WHERE customerid = i
       );
     END LOOP;
     COMMIT;
   END;
   /
   ```

   ![](12.png)

   向ORDERS表插入虚拟数据，具体流程如下：

   - 使用FOR循环语句，从1到30,000遍历每个整数i。
   - 在每个迭代中，使用INSERT INTO语句将一条新记录插入ORDERS表中，其中包含orderid（即i）、orderdate（在当前日期基础上随机减去1到365之间的天数）和customerid（在1到1000之间随机选择）。还使用SELECT语句从dual表中选择一行，同时使用子查询检查是否已经存在具有相同orderid的记录。如果存在，则不执行插入操作。
   - 不需要在循环外显式提交事务，因为每次循环都是一个单独的事务

   ````
   
   -- 插入虚拟数据到orders表
   BEGIN
     FOR i IN 1..30000 LOOP
       INSERT INTO orders (orderid, orderdate, customerid)
       SELECT i AS orderid, SYSDATE - TRUNC(DBMS_RANDOM.VALUE(1, 365)) AS orderdate, ROUND(DBMS_RANDOM.VALUE(1, 1000)) AS customerid
       FROM dual
       WHERE NOT EXISTS (
         SELECT 1
         FROM orders
         WHERE orderid = i
       );
     END LOOP;
     COMMIT;
   END;
   /
   ````

   ![](13.png)

   向ORDERTETAILS表插入虚拟数据，具体流程如下：

   - 使用FOR循环语句，从1到30,000遍历每个整数i。
   - 在每个迭代中，使用MERGE INTO语句将一条新记录插入ORDERTETAILS表中。首先，使用SELECT语句从dual表中选择一行，并生成具有以下字段的数据：orderid（即i）、productid（在1到30,000之间随机选择）、quantity（在1到10之间随机选择）和price（在10到100之间随机生成）。然后，使用MERGE INTO语句将这条数据与ORDERTETAILS表进行合并。如果存在orderid和productid都匹配的记录，则不执行任何操作。否则，插入一个新记录，其中包含orderid、productid、quantity和price。
   - 不需要在循环外显式提交事务，因为每次循环都是一个单独的事务。

   ```
   -- 插入虚拟数据到ORDERTETAILS表
   BEGIN
     FOR i IN 1..30000 LOOP
       MERGE INTO ORDERTETAILS od
       USING (
         SELECT i AS orderid, ROUND(DBMS_RANDOM.VALUE(1, 30000)) AS productid, ROUND(DBMS_RANDOM.VALUE(1, 10)) AS quantity, ROUND(DBMS_RANDOM.VALUE(10, 100), 2) AS price
         FROM dual
       ) data
       ON (od.orderid = data.orderid AND od.productid = data.productid)
       WHEN NOT MATCHED THEN
         INSERT (orderid, productid, quantity, price)
         VALUES (data.orderid, data.productid, data.quantity, data.price);
     END LOOP;
     COMMIT;
   END;
   /
   ```

   ![](14.png)

3. 建立用户和授权 

   ​		建立两个用户：管理员用户和普通用户。管理员用户拥有对所有表的完全访问权限，而普通用户只有部分访问权限。在授权时要考虑到安全性和灵活性。

   ```sql
   -- 创建用户
   CREATE USER sale_admin IDENTIFIED BY 123;
   -- 授予用户权限
   GRANT CONNECT, RESOURCE TO sale_admin;
   ```

   ![](1.png)

   ![](2.png)

   创建普通用户

   ```sql
   CREATE USER sale_user IDENTIFIED BY 123;
   ```

   ![](21.png)

4. 编写存储过程和函数 

   ​		创建一个名为sales_pkg的包，其中包含PROCEDURE子程序print_order_summary。此子程序将接受一个订单ID作为输入参数，然后打印关于该订单的信息，包括订单总金额、订单日期以及订单中每个商品的名称、数量和单价。

   ```
   CREATE OR REPLACE PACKAGE sales_pkg AS
     PROCEDURE print_order_summary(p_orderid IN NUMBER);
   END;
   /
   
   CREATE OR REPLACE PACKAGE BODY sales_pkg AS
     PROCEDURE print_order_summary(p_orderid IN NUMBER) IS
       l_order_total NUMBER := 0;
       l_order_date DATE;
       l_product_name VARCHAR2(100);
       l_quantity NUMBER;
       l_price NUMBER(10, 2);
     BEGIN
       -- 计算订单总金额
       SELECT SUM(od.quantity * p.price)
       INTO l_order_total
       FROM orders o
       JOIN ordertetails od ON o.orderid = od.orderid
       JOIN products p ON od.productid = p.productid
       WHERE o.orderid = p_orderid;
       
       -- 获取订单日期
       SELECT orderdate
       INTO l_order_date
       FROM orders
       WHERE orderid = p_orderid;
       
       -- 打印订单总金额和订单时间
       DBMS_OUTPUT.PUT_LINE('Order Total: $' || l_order_total);
       DBMS_OUTPUT.PUT_LINE('Order Date: ' || TO_CHAR(l_order_date, 'MM/DD/YYYY HH24:MI:SS'));
       
       -- 获取订单商品信息
       FOR r IN (
         SELECT p.productname, od.quantity, od.price
         FROM orders o
         JOIN ordertetails od ON o.orderid = od.orderid
         JOIN products p ON od.productid = p.productid
         WHERE o.orderid = p_orderid
       ) LOOP
         -- 打印每个商品的名称、数量和单价
         DBMS_OUTPUT.PUT_LINE(r.productname || ' (' || r.quantity || ') - $' || r.price);
       END LOOP;
     EXCEPTION
       WHEN NO_DATA_FOUND THEN
         DBMS_OUTPUT.PUT_LINE('Order not found');
     END;
   END;
   /
   ```

   ![](15.png)

   为验证程序包的可执行性。下面是程序包的简单调用示例：

   ```
   DECLARE
     P_ORDERID NUMBER;
   BEGIN
     P_ORDERID := 4332;
   
     SALES_PKG.PRINT_ORDER_SUMMARY(
       P_ORDERID => P_ORDERID
     );
   --rollback; 
   END;
   ```

   ![](16.png)

   ​		创建一个名为sales_pkg3的包，其中包含PROCEDURE子程序print_orders_by_customer。此子程序将接受一个顾客ID作为输入参数，然后按订单日期升序输出该顾客所有订单的摘要信息，包括每个订单的日期和总金额。

   ``` 
   CREATE OR REPLACE PACKAGE sales_pkg3 AS
     PROCEDURE print_orders_by_customer(p_customerid IN NUMBER);
   END;
   /
   
   CREATE OR REPLACE PACKAGE BODY sales_pkg3 AS
     PROCEDURE print_orders_by_customer(p_customerid IN NUMBER) IS
       l_orderdate DATE;
       l_order_total NUMBER;
     BEGIN
       FOR r IN (
         SELECT o.orderid, o.orderdate
         FROM orders o
         WHERE o.customerid = p_customerid
       ) LOOP
         -- 获取订单时间
         l_orderdate := r.orderdate;
         
         -- 计算订单总金额
         SELECT SUM(od.quantity * p.price)
         INTO l_order_total
         FROM ordertetails od
         JOIN products p ON od.productid = p.productid
         WHERE od.orderid = r.orderid;
         
         -- 打印订单时间和订单总金额
         DBMS_OUTPUT.PUT_LINE('Order Date: ' || TO_CHAR(l_orderdate, 'MM/DD/YYYY HH24:MI:SS'));
         DBMS_OUTPUT.PUT_LINE('Order Total: $' || l_order_total);
       END LOOP;
     EXCEPTION
       WHEN NO_DATA_FOUND THEN
         DBMS_OUTPUT.PUT_LINE('Orders not found');
     END;
   END;
   /
   ```

   ![](17.png)

   为验证程序包的可执行性。下面是程序包的简单调用示例：

   ```` 
   DECLARE
     P_CUSTOMERID NUMBER;
   BEGIN
     P_CUSTOMERID := 1234;
   
     SALES_PKG3.PRINT_ORDERS_BY_CUSTOMER(
       P_CUSTOMERID => P_CUSTOMERID
     );
   --rollback; 
   END;
   ````

   ![](18.png)

   ![](19.png)

   ​		

5. 设计备份方案 

   ​		为保证数据安全性和可恢复性，设计了一套数据库备份方案。首先将数据库切换到归档日志模式，然后使用使用RMAN备份整个数据库。然后定义了一段RMAN备份脚本，可以用于对整个数据库进行完整备份。该脚本包括了自动备份控制文件、备份集的保留策略、备份优化和删除过时备份集等设置。


```sql
-- 使用 SQL*Plus 登录到数据库
sqlplus / as sysdba

-- 关闭数据库
SHUTDOWN IMMEDIATE

-- 启动数据库实例并挂载数据库
STARTUP MOUNT

-- 将数据库切换到归档日志模式
ALTER DATABASE ARCHIVELOG；

-- 打开数据库
ALTER DATABASE OPEN；
```

![](22.png)

![](23.png)

```sql
rman target /
-- 显示 RMAN 的当前配置和参数设置
SHOW ALL;

-- 备份整个数据库（包括数据文件、控制文件和归档日志）
BACKUP AS COMPRESSED BACKUPSET DATABASE PLUS ARCHIVELOG;

-- 列出已备份的备份集信息
LIST BACKUP;
```

![](24.png)

![](26.png)



![](27.png)

```sql
# 用于完整数据库备份的RMAN备份脚本
# 设置日期格式
RUN {
  CONFIGURE CONTROLFILE AUTOBACKUP ON;
  CONFIGURE CONTROLFILE AUTOBACKUP FORMAT FOR DEVICE TYPE DISK TO '/backup/autobackup/%F';
  CONFIGURE RETENTION POLICY TO RECOVERY WINDOW OF 7 DAYS;
  CONFIGURE BACKUP OPTIMIZATION ON;

  # 备份整个数据库
  BACKUP AS COMPRESSED BACKUPSET DATABASE PLUS ARCHIVELOG DELETE INPUT;

  # 删除过时的备份
  CROSSCHECK BACKUP;
  DELETE NOPROMPT OBSOLETE;
}
```

## 总结与心得

​		通过这门课程的学习和实践，我对Oracle数据库的管理和维护有了更深入的理解和认识。本次实验基于Oracle数据库的商品销售系统设计，探讨了数据库设计、表空间设计、权限及用户分配、存储过程和函数设计以及数据库备份方案等方面。

​		在实验中，我不仅学习了数据库管理员应该具备的基本技能和知识，还深入了解了Oracle数据库的特点和优势。通过实践操作，我更加熟悉了数据库管理工具和命令，掌握了如何利用这些工具和命令来管理数据库并解决常见问题。最终，本次实验让我充分认识到数据库管理在企业中的重要性，并为我今后从事相关工作打下了坚实的基础。

​		总之，通过本次期末作业，我对Oracle数据库的操作和管理有了更深入的认识，不仅提高了我的实践能力和技能水平，还展现了我在数据库管理方面的性格特点和解决问题的能力。希望今后能够进一步学习和掌握相关知识，在数据库管理方面取得更多的进展。

