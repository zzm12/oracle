# 实验2：用户及权限管理

姓名：钟梓萌 学号:202010414428

## 实验目的

掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。

## 实验内容

Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

- 在pdborcl插接式数据中创建一个新的本地角色con_res_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有con_res_role的用户就同时拥有这三种权限。
- 创建角色之后，再创建用户sale，给用户分配表空间，设置限额为50M，授予con_res_role角色。
- 最后测试：用新用户sale连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。

## 实验参考步骤

对于以下的对象名称con_role，sale，在实验的时候应该修改为自己的名称。

- 第1步：以system登录到pdborcl，创建角色con_role和用户zzm，并授权和分配空间：

```sql
$ sqlplus system/123@pdborcl
CREATE ROLE con_role;
GRANT connect,resource,CREATE VIEW TO con_role;
CREATE USER zzm IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
ALTER USER zzm default TABLESPACE "USERS";
ALTER USER zzm QUOTA 50M ON users;
GRANT con_role TO zzm;
```

> 语句“ALTER USER zzmQUOTA 50M ON users;”是指授权zzm用户访问users表空间，空间限额是50M。

![](D:\大三下\Oracle\实验二\1.png)

![](D:\大三下\Oracle\实验二\2.jpg)

- 第2步：新用户zzm连接到pdborcl，创建表customers和视图customers_view，插入数据，最后将customers_view的SELECT对象权限授予hr用户。session_privs，session_roles可以查看会话权限和角色。

```sql
$ sqlplus zzm/123@pdborcl
SQL> show user;
USER is "sale"
SQL>
SELECT * FROM session_privs;
SELECT * FROM session_roles;
SQL>
CREATE TABLE customers (id number,name varchar2(50)) TABLESPACE "USERS" ;
INSERT INTO customers(id,name)VALUES(1,'zhang');
INSERT INTO customers(id,name)VALUES (2,'wang');
CREATE VIEW customers_view AS SELECT name FROM customers;
GRANT SELECT ON customers_view TO hr;
SELECT * FROM customers_view;
NAME
--------------------------------------------------
zhang
wang
```

![](D:\大三下\Oracle\实验二\3.jpg)

![](D:\大三下\Oracle\实验二\4.png)

![](D:\大三下\Oracle\实验二\5.jpg)

- 第3步：用户hr连接到pdborcl，查询sale授予它的视图customers_view

```sql
请输入用户名:  sys/zzm123432 as sysdba

连接到:
Oracle Database 11g Enterprise Edition Release 11.2.0.1.0 - 64bit Production
With the Partitioning, OLAP, Data Mining and Real Application Testing options

SQL>  alter user hr identified by hr account unlock;

用户已更改。

SQL> conn hr/hr
已连接。
SQL> SELECT * FROM zzm.customers_view
  2  ;

NAME
--------------------------------------------------
zhang
wang

SQL>
```

![](D:\大三下\Oracle\实验二\6.png)

## 数据库和表空间占用分析

> 当实验做完之后，数据库pdborcl中包含了新的角色con_res_role和用户sale。
> 新用户sale使用默认表空间users存储表的数据。
> 随着用户往表中插入数据，表空间的磁盘使用量会增加。

## 查看数据库的使用情况

以下样例查看表空间的数据库文件，以及每个文件的磁盘占用情况。

```sql
$ sqlplus system/zzm123432@pdborcl

SQL>SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files  WHERE  tablespace_name='USERS';

SQL>SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",
 free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
 Round(( total - free )/ total,4)* 100 "使用率%"
 from (SELECT tablespace_name,Sum(bytes)free
        FROM   dba_free_space group  BY tablespace_name)a,
       (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
        group  BY tablespace_name)b
 where  a.tablespace_name = b.tablespace_name;
```

![](D:\大三下\Oracle\实验二\7.jpg)

![](D:\大三下\Oracle\实验二\8.png)

- autoextensible是显示表空间中的数据文件是否自动增加。
- MAX_MB是指数据文件的最大容量。

## 实验结束删除用户和角色

```sh
$ sqlplus system/123@pdborcl
SQL>
drop role con_role;
drop user zzm;
```

![](D:\大三下\Oracle\实验二\9.png)

### 实验总结

在本次实验中，成功创建了用户zzm和对应的视图，并将试图权限赋予给hr用户进行查看。然后还对数据库的使用情况进行了查询。实验中需要注意的地方是hr账户是锁定的，我是通过sys登录然后对hr解开锁定，然后使用conn hr/hr完成的。
