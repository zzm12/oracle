# 实验1：SQL语句的执行计划分析与优化指导

姓名：钟梓萌  学号：202010414428

## 实验目的

  分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。

## 实验数据库和用户

  数据库是pdborcl，用户是sys和hr

## 实验内容

- 对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
- 设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，看看该工具有没有给出优化建议。



查询1：

```SQL
set autotrace on

SELECT e.employee_id, e.first_name, e.last_name, d.department_name
FROM hr.departments d,hr.employees e
WHERE e.department_id = d.department_id
AND e.salary > (SELECT AVG(salary) FROM hr.employees);

执行计划
----------------------------------------------------------
Plan hash value: 3864034822

--------------------------------------------------------------------------------

| Id  | Operation                    | Name        | Rows  | Bytes | Cost (%CPU)
| Time     |

--------------------------------------------------------------------------------

|   0 | SELECT STATEMENT             |             |     5 |   210 |     9  (12)
| 00:00:01 |

|   1 |  MERGE JOIN                  |             |     5 |   210 |     6  (17)
| 00:00:01 |

|   2 |   TABLE ACCESS BY INDEX ROWID| DEPARTMENTS |    27 |   432 |     2   (0)
| 00:00:01 |

|   3 |    INDEX FULL SCAN           | DEPT_ID_PK  |    27 |       |     1   (0)
| 00:00:01 |

|*  4 |   SORT JOIN                  |             |     5 |   130 |     4  (25)
| 00:00:01 |

|*  5 |    TABLE ACCESS FULL         | EMPLOYEES   |     5 |   130 |     3   (0)
| 00:00:01 |

|   6 |     SORT AGGREGATE           |             |     1 |     4 |
|          |

|   7 |      TABLE ACCESS FULL       | EMPLOYEES   |   107 |   428 |     3   (0)
| 00:00:01 |

--------------------------------------------------------------------------------
Predicate Information (identified by operation id):
---------------------------------------------------

   4 - access("E"."DEPARTMENT_ID"="D"."DEPARTMENT_ID")
       filter("E"."DEPARTMENT_ID"="D"."DEPARTMENT_ID")
   5 - filter("E"."SALARY"> (SELECT AVG("SALARY") FROM "HR"."EMPLOYEES"
              "EMPLOYEES"))

统计信息
----------------------------------------------------------
          8  recursive calls
          0  db block gets
         24  consistent gets
          0  physical reads
          0  redo size
       2594  bytes sent via SQL*Net to client
        552  bytes received via SQL*Net from client
          5  SQL*Net roundtrips to/from client
          1  sorts (memory)
          0  sorts (disk)
         50  rows processed
```

- 查询2

```SQL
set autotrace on
SELECT employees.employee_id, employees.first_name, employees.last_name, departments.department_name
FROM hr.employees
INNER JOIN hr.departments ON employees.department_id = departments.department_id
WHERE employees.salary > (SELECT AVG(salary) FROM hr.employees);

执行计划
----------------------------------------------------------
Plan hash value: 3864034822

--------------------------------------------------------------------------------

| Id  | Operation                    | Name        | Rows  | Bytes | Cost (%CPU)
| Time     |
--------------------------------------------------------------------------------

|   0 | SELECT STATEMENT             |             |     5 |   210 |     9  (12)
| 00:00:01 |

|   1 |  MERGE JOIN                  |             |     5 |   210 |     6  (17)
| 00:00:01 |

|   2 |   TABLE ACCESS BY INDEX ROWID| DEPARTMENTS |    27 |   432 |     2   (0)
| 00:00:01 |

|   3 |    INDEX FULL SCAN           | DEPT_ID_PK  |    27 |       |     1   (0)
| 00:00:01 |

|*  4 |   SORT JOIN                  |             |     5 |   130 |     4  (25)
| 00:00:01 |

|*  5 |    TABLE ACCESS FULL         | EMPLOYEES   |     5 |   130 |     3   (0)
| 00:00:01 |

|   6 |     SORT AGGREGATE           |             |     1 |     4 |
|          |

|   7 |      TABLE ACCESS FULL       | EMPLOYEES   |   107 |   428 |     3   (0)
| 00:00:01 |

--------------------------------------------------------------------------------
------------
Predicate Information (identified by operation id):
---------------------------------------------------

   4 - access("EMPLOYEES"."DEPARTMENT_ID"="DEPARTMENTS"."DEPARTMENT_ID")
       filter("EMPLOYEES"."DEPARTMENT_ID"="DEPARTMENTS"."DEPARTMENT_ID")
   5 - filter("EMPLOYEES"."SALARY"> (SELECT AVG("SALARY") FROM "HR"."EMPLOYEES"
              "EMPLOYEES"))
统计信息
----------------------------------------------------------
         10  recursive calls
          0  db block gets
         25  consistent gets
          0  physical reads
          0  redo size
       2594  bytes sent via SQL*Net to client
        552  bytes received via SQL*Net from client
          5  SQL*Net roundtrips to/from client
          1  sorts (memory)
          0  sorts (disk)
         50  rows processed

```
